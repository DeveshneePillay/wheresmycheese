package com.example.wheresmycheese

import com.mapbox.mapboxsdk.geometry.LatLng
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

const val RADIUS = 50

class ExampleUnitTest {

    val deviceLocation = LatLng(-29.7462137, 31.0481388)

    @Test
    fun isWithinRadius() {
        val cheeseLocation = LatLng( -29.746216, 31.048141)
        assertTrue(deviceLocation.distanceTo(cheeseLocation) <= RADIUS)
    }

    @Test
    fun notWithinRadius() {
        val cheeseLocation = LatLng(Constants.DEAFULT_LAT,Constants.DEAFULT_LNG)
        assertTrue(deviceLocation.distanceTo(cheeseLocation) > RADIUS)
    }
}