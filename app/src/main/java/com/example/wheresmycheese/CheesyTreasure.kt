package com.example.wheresmycheese

import com.mapbox.mapboxsdk.geometry.LatLng

const val RADIUS = 50
//const val RADIUS_TESTING = 500

class CheesyTreasure(val location: LatLng, val note: String?) {

    fun isWithinRadius(userLocation: LatLng): Boolean {
        return location.distanceTo(userLocation) <= RADIUS
    }
}