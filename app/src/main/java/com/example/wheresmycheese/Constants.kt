package com.example.wheresmycheese

object Constants {
    const val ACCESS_FINE_LOCATION = 16158
    const val ENABLE_LOCATION_REQUEST = 1
    const val REQUEST_LOCATION_ENABLED : Long = 10*1000
    const val REQUEST_UPDATE_INTERVAL_DEFAULT : Int = 1000
    const val REQUEST_UPDATE_INTERVAL_FAST : Int = 1000
    const val MAPBOX_API_TOKEN = "pk.eyJ1IjoiZGV2ZXNobmVlIiwiYSI6ImNrZWU4a3ludjBkdTAyeW43M3p1eXVubDQifQ.bwwSg8i6A8JpG9ZoQQ1rwA"
    const val DEAFULT_LAT = 31.0292
    const val DEAFULT_LNG = -29.8579
   
}