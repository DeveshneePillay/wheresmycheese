package com.example.wheresmycheese

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity() {

    private val markers: ArrayList<Marker> = arrayListOf()

    private lateinit var map: MapboxMap

    private var cheesyServiceBound = false
    private var cheesyService: CheesyService? = null
    private var cheesyServiceIntent: Intent? = null

    private val cheesyConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as CheesyService.CheesyBinder
            cheesyService = binder.service
        }

        override fun onServiceDisconnected(name: ComponentName) {
            cheesyService = null
            cheesyServiceBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timber.plant(Timber.DebugTree())
        startService()

        Mapbox.getInstance(this, Constants.MAPBOX_API_TOKEN)
        savedInstanceState?.let { mapView.onCreate(it) }
        checkPermissions()
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            checkLocationSettings()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), Constants.ACCESS_FINE_LOCATION
            )
        }
    }

    private fun checkLocationSettings() {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) enabledLocationRequest() else {
            initMap()
        }
    }

    private fun enabledLocationRequest() {
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = Constants.REQUEST_LOCATION_ENABLED
            fastestInterval = interval
        }
        val settingBuilder = LocationSettingsRequest.Builder().apply {
            addLocationRequest(locationRequest)
            setAlwaysShow(true)
        }
        val responseTask = LocationServices.getSettingsClient(this).checkLocationSettings(settingBuilder.build())
        responseTask.addOnCompleteListener { task ->
            try {
                task.getResult(ApiException::class.java)
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val resolvableApiException = e as ResolvableApiException
                        resolvableApiException.startResolutionForResult(
                            this@MainActivity,
                            Constants.ENABLE_LOCATION_REQUEST
                        )
                    } catch (ie: SendIntentException) {
                        ie.printStackTrace()
                    }
                }
            }
        }
    }

    private fun startService() {
        if (!cheesyServiceBound) {
            cheesyServiceIntent = Intent(this, CheesyService::class.java)
            cheesyServiceBound = bindService(cheesyServiceIntent, cheesyConnection, Context.BIND_AUTO_CREATE)
        }
    }

    private fun initMap() {
        mapView.getMapAsync {
            map = it
            setupLonPressListener()
            setUserLocation()
            map.setOnMarkerClickListener { marker ->
                showCheeseFoundDialog(marker)
                true
            }
        }
    }

    private fun setUserLocation(){
        val lastKnownLocation = cheesyService?.getLastKnownLocation()
        var lastKnownLatLng = LatLng(Constants.DEAFULT_LAT, Constants.DEAFULT_LNG)
        lastKnownLocation?.let {
            lastKnownLatLng = LatLng(it.latitude, it.longitude)
        }
        setCameraPosition(lastKnownLatLng)
    }

    private fun showCheeseFoundDialog(marker : Marker) {
        CheesyTreasureDialog(this, object : CheesyTreasureDialog.ITreasureDialogListener{
            override fun onCheeseCollected(note: String) {
                removeCheesyNote(marker)
            }
        }, marker.title).show()
    }

    private fun removeCheesyNote(marker: Marker){
        map.removeMarker(marker)
        markers.remove(marker)
        cheesyService?.removeCheesyTreasure(marker.title)
        showToast("Cheese Collected")
    }

    private fun setCameraPosition(latLng: LatLng) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, 15.0)
        )
        addCheeseToMap(point = latLng, iconId = R.drawable.ic_location_marker)
    }

    private fun setupLonPressListener() {
        map.setOnMapLongClickListener {
            createCheesyNote(it)
        }
    }

    private fun createCheesyNote(point: LatLng) {
        CheesyDialog(this, object : CheesyDialog.INoteDialogListener {
            override fun onNoteAdded(note: String) {
                addCheeseToMap(point = point, note = note, iconId = R.drawable.cheese32)
                saveCheesyNote(point, note)
            }
        }).show()
    }

    private fun addCheeseToMap(point: LatLng, note: String? = null, iconId: Int) {
        val iconFactory = IconFactory.getInstance(this@MainActivity)
        val markerIcon = iconFactory.fromBitmap(getBitmapFromDrawableId(iconId))
        markers.add(map.addMarker(MarkerOptions().apply {
            icon = markerIcon
            position = point
            title = note
        }))
    }

    private fun saveCheesyNote(point: LatLng, content: String) {
        cheesyService?.addCheesyTreasure(CheesyTreasure(point, content))
        showToast(getString(R.string.note_saved))
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }

    override fun onStop() {
        mapView.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        if (cheesyServiceBound) {
            unbindService(cheesyConnection)
            cheesyServiceBound = false
        }
        mapView.onDestroy()
        super.onDestroy()
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocationSettings()
                } else showToast(getString(R.string.location_permission_denied))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.ENABLE_LOCATION_REQUEST) initMap()
        else showToast(getString(R.string.location_permission_disabled))

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getBitmapFromDrawableId(drawableId: Int): Bitmap {
        val vectorDrawable = ContextCompat.getDrawable(this, drawableId)!!
        val wrapDrawable: Drawable = DrawableCompat.wrap(vectorDrawable)

        var h = vectorDrawable.intrinsicHeight
        var w = vectorDrawable.intrinsicWidth

        h = if (h > 0) h else 96
        w = if (w > 0) w else 96

        wrapDrawable.setBounds(0, 0, w, h)
        val bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bm)
        wrapDrawable.draw(canvas)
        return bm
    }
}