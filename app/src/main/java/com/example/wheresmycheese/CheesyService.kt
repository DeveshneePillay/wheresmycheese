package com.example.wheresmycheese

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.services.android.telemetry.location.LocationEngine
import com.mapbox.services.android.telemetry.location.LocationEngineListener
import com.mapbox.services.android.telemetry.location.LocationEnginePriority
import com.mapbox.services.android.telemetry.location.LocationEngineProvider
import timber.log.Timber

class CheesyService : Service(), LocationEngineListener {

    var cheesyTreasures: ArrayList<CheesyTreasure> = arrayListOf()
    private var locationEngine: LocationEngine? = null
    private var notificationManager: NotificationManager? = null
    private var cheesyBinder: CheesyBinder? = null

    override fun onBind(intent: Intent): IBinder? {
        return cheesyBinder
    }

    override fun onCreate() {
        super.onCreate()
        initLocationEngine()
        cheesyBinder = CheesyBinder()
        createServiceNotification()
    }

    private fun createServiceNotification() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_MIN
            ).apply {
                description = getString(R.string.notification_cheese_description)
            }
            notificationManager?.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID).apply {
            setSmallIcon(R.drawable.cheese32)
            setContentTitle(getString(R.string.app_name))
            setContentText(getString(R.string.notification_cheese_description))
            setOngoing(true)
            priority = NotificationCompat.PRIORITY_MIN
        }
        notificationManager?.notify(NOTIFICATION_SERVICE_ID, notification.build())
    }

    /* interval & fastInterval set to 1 second for testing purposes. Consideration to be made for device batter, memory and data
       smallestDisplacement is set to 0 for testing purposes - should be set according to average walking/moving speed
     */
    @SuppressLint("MissingPermission")
    private fun initLocationEngine() {
        locationEngine = LocationEngineProvider(applicationContext).obtainBestLocationEngineAvailable()
        locationEngine?.apply {
            priority = LocationEnginePriority.HIGH_ACCURACY
            interval = Constants.REQUEST_UPDATE_INTERVAL_DEFAULT
            fastestInterval = Constants.REQUEST_UPDATE_INTERVAL_FAST
            smallestDisplacement = 0F
            addLocationEngineListener(this@CheesyService)
            activate()
            requestLocationUpdates()
            Timber.d("WMC : LOCATION_ENGINE_RUNNING")
        }
    }

    override fun onLocationChanged(location: Location?) {
        Timber.d("LOCATION_UPDATE : LAT:${location?.latitude}, LONG : ${location?.longitude}")
        checkForNearbyCheese(location)?.let { foundCheeses ->
            Timber.d("WMC : CHEESE FOUND")
            if (foundCheeses.isNotEmpty()) {
                foundCheeses.forEach { cheese ->
                    cheese.note?.let { createCheeseFoundNotification(cheese) }
                }
            }
        }
    }

    private fun checkForNearbyCheese(location: Location?): List<CheesyTreasure>? {
        val cheeseFound = mutableListOf<CheesyTreasure>()
        cheesyTreasures.forEach { cheese ->
            location?.let {
                val deviceLocation = LatLng(it.latitude, it.longitude)
                if (cheese.isWithinRadius(deviceLocation)) cheeseFound.add(cheese)
            }
        }
        return cheeseFound
    }

    private fun createCheeseFoundNotification(cheese: CheesyTreasure) {

        val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID).apply {
                setSmallIcon(R.drawable.cheese32)
                setContentTitle(getString(R.string.notification_cheese_found))
                setContentText(cheese.note)
                priority = NotificationCompat.PRIORITY_DEFAULT
            }
        notificationManager?.notify(NOTIFICATION_CHEESE_FOUND, notification.build())
    }

    override fun onConnected() {
    }

    @SuppressLint("MissingPermission")
    fun getLastKnownLocation(): Location? = locationEngine?.lastLocation

    fun addCheesyTreasure(treasure: CheesyTreasure) {
        cheesyTreasures.add(treasure)
        Timber.d("WMC : CHEESE ADDED")
    }

    fun removeCheesyTreasure(cheesyNote: String) {
        cheesyTreasures.forEach { treasure ->
            if (treasure.note.equals(cheesyNote, true)) {
                cheesyTreasures.remove(treasure)
            }
        }
        notificationManager?.cancel(NOTIFICATION_CHEESE_FOUND)
        Timber.d("WMC : CHEESE REMOVED")
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        locationEngine?.removeLocationUpdates()
        notificationManager?.cancelAll()
        stopSelf()
    }

    inner class CheesyBinder : Binder() {
        val service = this@CheesyService
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "cheesy_service"
        private const val NOTIFICATION_CHANNEL_NAME = "wheres_my_cheese"
        private const val NOTIFICATION_SERVICE_ID = 1234
        private const val NOTIFICATION_CHEESE_FOUND = 5678
    }
}
