package com.example.wheresmycheese

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.dialog_note.*

class CheesyDialog (context: Context, private val listener: INoteDialogListener) : Dialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.dialog_note)
        saveCheeseButton.setOnClickListener(this)
        exitDialogButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
       view?.let {
           when(it.id){
               R.id.saveCheeseButton -> {
                 listener.onNoteAdded(noteText.text.toString())
                   dismiss()
               }
               R.id.exitDialogButton -> {
                   dismiss()
               }
           }
       }
    }

    interface INoteDialogListener{
        fun onNoteAdded(note : String)
    }
}