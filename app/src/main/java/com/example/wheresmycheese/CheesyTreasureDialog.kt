package com.example.wheresmycheese

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.dialog_treasure.*

class CheesyTreasureDialog(context: Context, private val listener: ITreasureDialogListener, private val note : String) :
    Dialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.dialog_treasure)
        cheeseNote.text = note
        collectCheeseButton.setOnClickListener(this)
        closeTreasureDialogButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        view?.let {
            when (it.id) {
                R.id.collectCheeseButton -> {
                    listener.onCheeseCollected(note)
                    dismiss()
                }
                R.id.closeTreasureDialogButton -> {
                    dismiss()
                }
            }
        }
    }

    interface ITreasureDialogListener {
        fun onCheeseCollected(note : String)
    }
}